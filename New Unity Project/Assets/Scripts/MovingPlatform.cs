﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    //moving platform tutorial at bottom
    
    public Vector3[] points;
    public int point_number = 0;
    private Vector3 current_target;

    public float tolerance;
    public float speed;
    public float delay_time;

    private float delay_start;


    void Start()
    {
        if (points.Length > 0)
        {
            current_target = points[0];
        }
        tolerance = speed * Time.deltaTime;
    }

    void Update()
    {
        if (transform.position != current_target)
        {
            moveWall();
        }
        else
        {
            updateTarget();
        }
    }

    void moveWall()
    {
        Vector3 heading = current_target - transform.position;
        transform.position += (heading / heading.magnitude) * speed * Time.deltaTime;
        if (heading.magnitude < tolerance)
        {
            transform.position = current_target;
            delay_start = Time.time;
        }
    }

    void updateTarget()
    {
        if (Time.time - delay_start > delay_time)
        {
            nextPlatform();
        }
    }

    void nextPlatform()
    {
        point_number++;
        if (point_number >= points.Length)
        {
            point_number = 0;
        }
        current_target = points[point_number];
    }
}


/*
 * moving platform tutorial:
 * https://www.youtube.com/watch?v=9KdY4mafG_E&ab_channel=Dekoba
 */