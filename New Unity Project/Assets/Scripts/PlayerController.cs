﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Security.Cryptography;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private int count;
    private Rigidbody rb;
    private float movementX;
    private float movementY;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        setCountText();
        winTextObject.SetActive(false);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;

    }

    void OnJump()
    {
        if (transform.position.y <= 1)
        {
            rb.AddForce(0, 5, 0, ForceMode.Impulse);
        }
    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 10)
        {
            winTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            setCountText();
        }
        else if (other.gameObject.CompareTag("Powerup"))
        {
            other.gameObject.SetActive(false);
            speed = speed * 2;
        }
    }
}


//https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/
//timer tutorial I used^^